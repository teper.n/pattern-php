<?php

interface Connection {
    public function request(string $url, array $options): void;
}

class XMLHttpService implements Connection {

    public function request($url, $options): void {
        //
    }
}

class Http {
    private $service;

    public function __construct(Connection $connection) 
    {
        $this->service = $connection;
    }

    public function get(string $url, array $options): void 
    {
        $this->service->request($url, $options);
    }

    public function post(string $url, array $options): void 
    {
        $this->service->request($url, $options);
    }
}
