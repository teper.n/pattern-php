<?php

namespace Task;

class Concept {

    private $client;

    public function __construct() 
    {
        $this->client = new \GuzzleHttp\Client();
    }

    public function getUserData() 
    {
        $params = [
            'auth' => ['user', 'pass'],
            'token' => $this->getSecretKey()
        ];

        $request = new \Request('GET', 'https://api.method', $params);
        $promise = $this->client->sendAsync($request)->then(function ($response) {
            $result = $response->getBody();
        });

        $promise->wait();
    }
    
    /**
     * Сохранение токена.
     * 
     * @param TokenRequest $request
     * 
     * @throws \JsonException
     * @return void
     */
    
    public function getSecretKey(TokenRequest $request): void 
    {
        $save_type = $request;

        try {
            switch ($save_type->saving_type) {
                case 'file':
                    $user_token = new FileToken();
                    break;
                case 'db':
                    $user_token = new DbToken();
                    break;
                case 'redis':
                    $user_token = new RedisToken();
                    break;
                case 'cloud':
                    $user_token = new CloudToken();
                    break;
                default:
                    throw new \RuntimeException('Ошибка при сохранении токена, не найден тип', 500);
            }
        } catch (\Exception $e) {
            throw new \RuntimeException('Ошибка при сохранении токена', 500, $e);
        }
    }
}