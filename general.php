<?php
$array = [
  ['id' => 1, 'date' => "12.01.2020", 'name' => "test1", 'color' => "red"],
  ['id' => 2, 'date' => "02.05.2020", 'name' => "test2", 'color' => "red"],
  ['id' => 4, 'date' => "08.03.2020", 'name' => "test4", 'color' => "red"],
  ['id' => 1, 'date' => "22.01.2020", 'name' => "test1", 'color' => "black"],
  ['id' => 2, 'date' => "11.11.2020", 'name' => "test4", 'color' => "red"],
  ['id' => 3, 'date' => "06.06.2020", 'name' => "test3", 'color' => "red"]
];

/*1. выделить уникальные записи (убрать дубли) в отдельный массив. 
В конечном массиве не должно быть элементов с одинаковым id.*/
$keys = array_column($array, 'id');
$array1 = array_values(array_combine($keys, $array));
$array2 = array_map('unserialize', array_diff(array_map('serialize', $array), array_map('serialize', $array1)));

/*2. отсортировать многомерный массив по ключу (любому) */
$sort = $array;
array_multisort($keys, SORT_DESC, $sort);

/*3. вернуть из массива только элементы, удовлетворяющие внешним условиям (например элементы с определенным id) */
$cond = 1;

$result = array_filter($array, function($v) use($cond) { 
   return $v['id'] == $cond;
});

/*4. изменить в массиве значения и ключи (использовать name => id в качестве пары ключ => значение)*/  
$array_keys = array_column($array, 'name');
$array_values = array_column($array, 'id');
$res = array_combine(array_unique($array_keys), array_unique($array_values));

/*5. В базе данных имеется таблица с товарами goods (id INTEGER, name TEXT), таблица с тегами tags (id INTEGER, name TEXT) и таблица связи товаров и тегов goods_tags (tag_id INTEGER, goods_id INTEGER, UNIQUE(tag_id, goods_id)). Выведите id и названия всех товаров, которые имеют все возможные теги в этой базе
*/

/*SELECT `goods_id` AS `id`, `goods`.`name` FROM `goods_tags` LEFT JOIN `goods` ON `goods_tags`.`goods_id` = `goods`.`id` GROUP BY `goods_id` HAVING count(*) = (SELECT COUNT(`tags`.`id`) FROM `tags`) */

/*6. Выбрать без join-ов и подзапросов все департаменты, в которых есть мужчины, и все они (каждый) поставили высокую оценку (строго выше 5).*/

/*SELECT `department`.`name` FROM `evaluations`, `department` WHERE `evaluations`.`department_id` = `department`.`id` GROUP BY `department`.`name` HAVING (AVG(`evaluations`.`gender`)>0 AND AVG(`evaluations`.`value`)>5) */
